# A2232 #

This repository contains Kicad project files for A2232 USB adaptor 
board, based on FTDI FT2232 part. This board is considered as an test 
platform for FT2232H. Please refer to the part datasheet 
supplied by the manufacturer.

###LICENSE###

This design is licensed under Creative Commons Attribution 4.0 International (CC BY 4.0)

You are free to:
    Share — copy and redistribute the material in any medium or format
    Adapt — remix, transform, and build upon the material  for any purpose, even commercially. 
